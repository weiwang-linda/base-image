#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

while test $# -gt 1; do
  case $1 in
    -c|--stress-cmd)
      STRESS_CMD=$(printf "$2" $(nproc) $mem_avail)
      shift 2
      ;;
    -a|--container-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift 2
      ;;
    -o|--orderly-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      shift 2
      ;;
    -c|--confusion-opts)
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift 2
      ;;
    --cpuset)
      CPUSET="1"
      shift
      ;;
  esac
done

if test -z "$STRESS_CMD"; then
  error "No stress command given!"
  exit 1
fi

ioping="http://ftp.halifax.rwth-aachen.de/fedora-epel/8/Everything/$(uname -m)/Packages/i/ioping-1.1-1.el8.$(uname -m).rpm"

test_image=$(build_container_image stress-ng $ioping)

echo "Starting first run"
podman run $CONTAINER_PARAMS $ORDERLY_OPTS --name orderly $test_image ioping -B -c 5 -i 1 -q -S 100m /root > run1.log

echo "Starting interference using: $STRESS_CMD"
podman run $CONFUSION_OPTS --name confusion $test_image $STRESS_CMD >/dev/null 2>&1 &
# Give stress-ng a moment to start
sleep 2
echo "Starting second run"
podman run $CONTAINER_PARAMS $ORDERLY_OPTS --name orderly $test_image ioping -B -c 5 -i 1 -q -S 100m /root > run2.log

avg1=$(cat run1.log | cut -d ' ' -f6)
avg2=$(cat run2.log | cut -d ' ' -f6)

mdev1=$(cat run1.log | cut -d ' ' -f8)
mdev2=$(cat run2.log | cut -d ' ' -f8)


if test "$mdev1" -gt "$avg1" || test "$mdev2" -gt "$avg2"; then
  error "Standard deviation larger than average! Aborting.."
  exit 1
fi

max_rel_error=$(( avg1 / 5 ))

avg_difference=$(( avg1 - avg2 ))
avg_difference=${avg_difference#-}  # abs()

echo "First  run I/O latency is $((avg1 / 1000))us"
echo "Second run I/O latency is $((avg2 / 1000))us"
if test $avg_difference -gt $max_rel_error; then
    error "Interference is present!"
    exit 1
else
    success "No interference!"
fi

