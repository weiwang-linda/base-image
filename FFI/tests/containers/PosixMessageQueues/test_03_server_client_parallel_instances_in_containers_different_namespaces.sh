#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running posix_message_queues test in server container."
podman run $CONTAINER_PARAMS -d --name server $BASE_CONTAINER_IMAGE ./tst_posix_message_queues > /dev/null

printf "%s\n" "-- Running posix_message_queues test in client container. Expected: failure, not able to access the same queue."
podman run $CONTAINER_PARAMS --name client $BASE_CONTAINER_IMAGE ./tst_posix_message_queues > /dev/null

