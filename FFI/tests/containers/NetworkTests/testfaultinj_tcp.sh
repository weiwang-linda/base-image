#!/bin/bash
if test -z "$1"; then
    echo "Failed to run test, invalid fault command!"
    exit 1
fi
acceptableloss=0
modprobe --verbose sch_netem
if test "$( rpm --query --queryformat="%{VERSION}-%{RELEASE}.%{ARCH}\n" kernel-modules-extra)" != "$(uname -r)"; then
    echo "no match found for installing kernel-modules-extra"
    echo "Aborting the Test"
    exit 1
fi
if ! modprobe -q sch_netem; then
  echo "error loading sch_netem module"
  echo "Aborting the Test"
  exit 1
fi
podman run -d --name orderly stream9-stress
podman run -d --cap-add=NET_ADMIN --name confusion stream9-stress
podman run -d --name partner stream9-stress
partner=$(podman inspect partner | grep IPAddress | tail -1 | awk '{print $NF}' |  sed 's/"//g' | sed 's/,//g')
confusion=$(podman inspect confusion | grep IPAddress | tail -1 | awk '{print $NF}' |  sed 's/"//g' | sed 's/,//g')
orderly=$(podman inspect orderly | grep IPAddress | tail -1 | awk '{print $NF}' |  sed 's/"//g' | sed 's/,//g')
FAULT_CMD="$1"
podman exec -it confusion sh -c "$FAULT_CMD"
podman exec -it confusion sh -c "tc qdisc show"

#Collecting metrics after injecting the fault#

echo -e "\e[1;33m statistics after injecting the fault : \e[0m"
confusionploss=$(podman exec -it confusion sh -c "mtr --tcp --no-dns --report $confusion")
plossconfusiontcp=$(echo "$confusionploss" | tail -1 |  awk '{print $3}' | sed 's/%//g')
echo "packet loss at confusion for tcp is $plossconfusiontcp"
orderlyploss=$(podman exec -it orderly sh -c "mtr --tcp --no-dns --report $orderly")
plossorderlytcp=$(echo "$orderlyploss" | tail -1 |  awk '{print $3}' | sed 's/%//g')
echo "packet loss at orderly for tcp is $plossorderlytcp"
sleep 3
podman exec -it partner "qperf" &
confusionpdelay=$(podman exec -it confusion sh -c "qperf -v $partner tcp_lat")
confusionlattcp=$(echo "$confusionpdelay" | sed -n '2p' | tail -1 | awk '{print $3}')
echo "at confusion latency for tcp is $confusionlattcp"
orderlypdelay=$(podman exec -it orderly sh -c "qperf -v $partner tcp_lat")
orderlylattcp=$(echo "$orderlypdelay" | sed -n '2p' | tail -1 | awk '{print $3}')
echo "at orderly latency for tcp is $orderlylattcp"
confusionlattcp=$(echo "$confusionlattcp*1000" | bc )
confusionlatustcp=$(echo "$confusionlattcp*0.05" | bc )
confusionretrans=$(podman exec -it confusion sh -c "netstat -s  | grep retransmitted")
retranscon=$(echo "$confusionretrans" | tail -1 |awk '{print $1}')
echo "retransmitted segments at Confusion are $retranscon"
orderlyretrans=$(podman exec -it orderly sh -c "netstat -s  | grep retransmitted")
retransord=$(echo "$orderlyretrans" | tail -1 |awk '{print $1}')
echo "retransmitted segments at orderly are $retransord"

#Determining pass fail for Retransmissions, Packet loss and Packet delay#

echo -e "\e[1;33m Evaluating... \e[0m"
echo ""
if awk "BEGIN {exit !($retransord > $retranscon)}"; then
  echo -e "\e[1;31m Interference is present - presence of packet retransmission in Orderly \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No packet retransmission in Orderly \e[0m"
fi
if awk "BEGIN {exit !($plossorderlytcp > $acceptableloss)}"; then
  echo -e "\e[1;31m Interference is present - presence of packet loss in Orderly for tcp \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No loss in Orderly for tcp\e[0m"
fi
if awk "BEGIN {exit !($orderlylattcp > $confusionlatustcp)}"; then
  echo -e "\e[1;31m Interfernce is present - presence of packet delay in Orderly for tcp \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No packet delay in Orderly for tcp\e[0m"
fi
./cleanup.sh
