#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

make PREFIX=tst_sys

image=$(build_container_image gcc)
podman run $CONTAINER_PARAMS $image make
