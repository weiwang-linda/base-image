#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running system_v_semaphore test in server container."
podman run $CONTAINER_PARAMS -d --name server $BASE_CONTAINER_IMAGE ./tst_system_v_semaphore > /dev/null

printf "%s\n" "-- Running system_v_semaphore test in client container using the same namespace as a server. Expected: success."
podman run $CONTAINER_PARAMS --name client --ipc container:server $BASE_CONTAINER_IMAGE ./tst_system_v_semaphore > /dev/null

