#!/bin/bash

set -eux

if [[ "${STREAM:-}" != "downstream" ]]; then
  exit 0
fi

distro="${PRODUCT_BUILD_PREFIX}"
karch="$(arch)"

echo "Add ${distro} repos"

cat <<EOF > "/etc/yum.repos.d/${distro}.repo"
[${distro}]
name=${distro}
baseurl=${CI_REPOS_ENDPOINT}/${UUID}/repos/${COMPOSE_DISTRO}/compose/${COMPOSE_DISTRO}/${karch}/os/
enabled=1
gpgcheck=0
priority=10
EOF
