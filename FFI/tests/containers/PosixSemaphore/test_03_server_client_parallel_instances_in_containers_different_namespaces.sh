#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <semaphore-name> <timeout-seconds>"
    exit 1
fi

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running named_posix_semaphore test in server container."
podman run $CONTAINER_PARAMS -d --name server $BASE_CONTAINER_IMAGE ./tst_named_posix_semaphore "$1" "$2" > /dev/null

sleep 2

printf "%s\n" "-- Running named_posix_semaphore test in client container."
podman run $CONTAINER_PARAMS --name client $BASE_CONTAINER_IMAGE ./tst_named_posix_semaphore "$1" "$2" > /dev/null

