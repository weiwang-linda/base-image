#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

if ! test -x tst_sys_shm; then
  gcc -o tst_sys_shm -lrt shm.c -Wall -Werror
fi

if ! test -x tst_shm; then
  image=$(build_container_image gcc)
  podman run $CONTAINER_PARAMS $image gcc -o tst_shm -lrt shm.c -Wall -Werror
fi

