#!/bin/bash

set -uxo pipefail

#print for debug
ostree admin status

# Check current sha in ostree identical 
# To saved file
IMAGE_SHA_ID=$(ostree admin status | grep '\*' | awk '{print $3}')

grep "$IMAGE_SHA_ID" /root/.UPDATED_SHA_ID 

if [ "$?" != 0 ]; then
  echo "$IMAGE_SHA_ID is not as expected" >&2
  exit 1
fi

