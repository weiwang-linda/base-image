#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <semaphore-name> <timeout-seconds>"
    exit 1
fi

. $(dirname $(readlink --canonicalize $0))/setup.sh


printf "%s\n" "-- Running named_posix_semaphore test as a server (detached mode container mode)."
podman run $CONTAINER_PARAMS -d --name confusion $BASE_CONTAINER_IMAGE ./tst_named_posix_semaphore "$1" "$2" > /dev/null
sleep 2

printf "%s\n" "-- Running named_posix_semaphore as a client in system. Expected: failure, not able to access the same semaphore."
./tst_sys_named_posix_semaphore "$1" "$2" 
