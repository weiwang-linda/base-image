# Freedom From Interference (FFI) - test impact caused by memory fragmentation

In theory containers or regular applications can fragment the physical memory space over time. To find out the impact on other applications the impact on memory initialization is tested here in three tests.

In all cases the memory fragmentation tool will occupy about half of the memory when the second measurement is taken. So the difference between cases 1+2 and 3 is that in case 3 the physical memory space is not fragmented anymore (while the fragmentation logic's virtual space still will be).

## Test cases

1. tc01_fragment-system-and-allocate
   1. Start one container called orderly
   1. Measure initialization duration of some chunk of memory in orderly
   1. Fragment the system's memory
   1. Measure initialization duration of some chunk of memory in orderly

2. tc02_fragment-container-and-allocate
   1. Start two containers: confusion and orderly
   1. Measure initialization duration of some chunk of memory in orderly
   1. Fragment the system's memory from within confusion
   1. Measure initialization duration of some chunk of memory in orderly

3. tc03_fragment-container-and-run-compaction-and-allocate
   1. Start two containers: confusion and orderly
   1. Measure initialization duration of some chunk of memory in orderly
   1. Fragment the system's memory from within confusion
   1. Trigger a manual memory compaction run
   1. Measure initialization duration of some chunk of memory in orderly

