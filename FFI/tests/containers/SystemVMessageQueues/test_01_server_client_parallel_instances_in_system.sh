#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running system_v_message_queues as server."
./tst_sys_system_v_message_queues >/dev/null &

printf "%s\n" "-- Running system_v_message_queues as client. Expected: success."
./tst_sys_system_v_message_queues 
