#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

if ! test -x tst_sys_named_posix_semaphore; then
  gcc -o tst_sys_named_posix_semaphore -lrt named_posix_semaphore.c -Wall -Werror
fi

if ! test -x tst_named_posix_semaphore; then
  image=$(build_container_image gcc)
  podman run $CONTAINER_PARAMS $image gcc -o tst_named_posix_semaphore -lrt named_posix_semaphore.c -Wall -Werror
fi

