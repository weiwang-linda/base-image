#!/bin/bash

set -eE

if test ${#BASH_SOURCE[@]} -lt 2; then
    error "Do not call this directly!"
    exit 1
fi

RED='\033[0;31m'
GRN='\033[0;32m'

error() {
    __output 2 $RED "$*"
}

success() {
    __output 1 $GRN "$*"
}

__output() {
    fd="$1"
    color="$2"
    shift 2
    msg="$*"
    if test -t 1; then
        printf "$color%s\033[0;0m\n" "$msg" >&$fd
    else
        printf "%s\n" "$msg" >&$fd
    fi
}

__random() {
    shuf -ern 10 {a..z} | tr -d '\n'
}

__cleanup() {
  # TODO: test if that base image was there before
    if test -n "$__helpers"; then
        for helper in $__helpers; do
            if test "$helper" != "$BASE_CONTAINER_IMAGE"; then
                podman rmi -f $helper
            fi
        done
    fi

    podman rm -fi orderly
    podman rm -fi confusion
    # podman's main process exits sometimes before cleanup finished
    sleep 0.2
}

__exit_fail() {
    __cleanup
    exit 1
}

build_container_image() {
    if test $# -lt 1; then
        __helper="$BASE_CONTAINER_IMAGE"
    else
        __requirements="$*"
        # TODO: Add a collision check?
        __helper=$(__random)
        cat <<EOF | podman build --quiet --tag $__helper -f - >/dev/null
FROM $BASE_CONTAINER_IMAGE

RUN dnf -y install $__requirements && dnf clean all

EOF
    fi

    __helpers+="$__helper "
    echo $__helper
}

# arg1: name of container
# arg2: output to wait for
# arg3: (optional) success message
# arg4: (optional) timeout
poll_container_for_string() {
    container="$1"
    search_string="$2"
    message="Success"
    timeout=60

    test -n "$3" && message="$3"
    test -n "$4" && timeout="$4"

    seconds_start=$(date +%s)
    while true; do
        seconds_end=$(date +%s)
        elapsed=$(( seconds_end - seconds_start ))
        if podman logs --tail 1 confusion | grep -q "$search_string"; then
            echo "$message after ${elapsed}s"
            return 0
        fi
        if test $elapsed -ge $timeout; then
            return 1
        fi
        sleep 1
    done
}

chcon -R -t container_file_t .

CONTAINER_PARAMS="--rm -t -v $PWD:$PWD -w $PWD"

trap "echo 'Caught signal! Exiting..'; __exit_fail" ERR SIGINT
trap "echo 'Terminating..'; __cleanup" 0

printf "%s\n" "-- Starting test ${BASH_SOURCE[1]}"

