#!/bin/bash

# Run once for debugging logs
dmesg -r -l crit,alert,emerg

# Count the number of lines
if [ "$(dmesg -r -l crit,alert,emerg | wc -l)" -gt 0 ]; then
  >&2 echo "Errors in dmesg detected marked as critical, alert, or emergency"
  exit 1
fi
