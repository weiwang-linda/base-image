#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

mem_avail=$(($(grep Available /proc/meminfo | awk '{ print $2 }') * 85 / 100 ))

while test $# -gt 1; do
  case $1 in
    -c|--stress-cmd)
      STRESS_CMD=$(printf "$2" $(nproc) $mem_avail)
      shift 2
      ;;
    -a|--container-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift 2
      ;;
    -o|--orderly-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      shift 2
      ;;
    -c|--confusion-opts)
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift 2
      ;;
    --cpuset)
      CPUSET="1"
      shift
      ;;
  esac
done

if test -z "$STRESS_CMD"; then
  error "No stress command given!"
  exit 1
fi

if test -n "$CPUSET"; then
  NPROC=$(nproc)
  if test $NPROC -lt 2; then
    error "Not using core pinning, only one detected"
    error "Skipping test!"
    exit 0
  else
    ORDERLY_OPTS="$ORDERLY_OPTS --cpuset-cpus=0"
    if test $NPROC -eq 2; then
      CONFUSION_OPTS="$CONFUSION_OPTS --cpuset-cpus=1"
    else
      CONFUSION_OPTS="$CONFUSION_OPTS --cpuset-cpus=1-$((NPROC - 1))"
    fi
  fi
fi

if ! test -f tst_prime; then
  echo "Building helper image"
  build_image=$(build_container_image gcc)

  echo "Compiling test"
  podman run $CONTAINER_PARAMS $build_image gcc -o tst_prime prime.c -Wall -Werror
fi

test_image=$(build_container_image time stress-ng)

echo "Starting first run"
podman run $CONTAINER_PARAMS $ORDERLY_OPTS --name orderly $test_image \time -v ./tst_prime > run1.log

echo "Starting interference using: $STRESS_CMD"
podman run $CONFUSION_OPTS --name confusion $test_image $STRESS_CMD >/dev/null 2>&1 &
# Give stress-ng a moment to start
sleep 2
echo "Starting second run"
podman run $CONTAINER_PARAMS --name orderly $test_image \time -v ./tst_prime > run2.log

# set to base 10 because the leading '0' might confuse the shell
run_time1=10#$(grep Elapsed run1.log | awk '{ print $NF }' | cut -d ':' -f2 | sed 's/\.//' | tr -d '\r')
run_time2=10#$(grep Elapsed run2.log | awk '{ print $NF }' | cut -d ':' -f2 | sed 's/\.//' | tr -d '\r')

max_rel_error=$(( run_time1 / 5 ))

run_time_difference=$(( run_time1 - run_time2 ))
run_time_difference=${run_time_difference#-}  # abs()

echo "First run took $((run_time1 / 100))s and the second run took $((run_time2 / 100))s"
if test $run_time_difference -gt $max_rel_error; then
    error "Interference is present!"
    exit 1
else
    success "No interference!"
fi

